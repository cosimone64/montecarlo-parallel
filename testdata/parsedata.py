#!/bin/env python
import sys

if len(sys.argv) != 2:
    sys.stderr.write('Use as ' + sys.argv[0] + ' <filename>\n')
    exit(-1)

with open(sys.argv[1], 'r') as inputfile:
    workernum = 1;
    for line in [x for x in inputfile.readlines() if x.startswith('Time')]:
        time_in_microseconds = line.split(':')[-1].strip()
        print(str(workernum) + '\t' + time_in_microseconds)
        workernum = workernum + 1 if workernum == 1 else workernum + 2
