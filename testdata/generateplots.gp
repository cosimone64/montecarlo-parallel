#!/bin/env gnuplot

set terminal pngcairo enhanced size 600,600
set grid
set autoscale
set xtic auto
set ytic auto
set xlabel '{/*1.5 Number of workers}'
set xrange [0:250]
sequentialtime = 5017068751.0
oneworkerthreadtime = 5163164050.0
oneworkerfftime = 6223410861.0

set style line 1 lt -1 pi -4 pt 6 lw 1 lc rgb "black"
set style line 2 dt 4 pi -2 pt 6 lw 1 lc rgb "black"

set ylabel '{/*1.5 Time (in seconds)}'

set yrange [0:3000]
set output 'threadtime.png'
plot 'threadplotdata.dat' u 1:($2/1000000) title 'Thread version completion time  - actual'\
w lp ls 1,\
sequentialtime/1000000/x title 'Thread version completion time - ideal' ls 2

set output 'fftime.png'
plot 'ffplotdata.dat' u 1:($2/1000000) title 'FastFlow version completion time - actual'\
w lp ls 1,\
sequentialtime/1000000/x title 'FastFlow version completion time -ideal' ls 2


set yrange [0:100]
set ylabel '{/*1.5 Speedup}';

set output 'threadspeedup.png'
plot 'threadplotdata.dat' u 1:(sequentialtime/$2) title 'Thread version speedup - actual'\
w lp ls 1,\
x title 'Thread version speedup - ideal' ls 2

set output 'ffspeedup.png'
plot 'ffplotdata.dat' u 1:(sequentialtime/$2) title 'FastFlow version speedup - actual'\
w lp ls 1,\
x title 'FastFlow version speedup - ideal' ls 2


set ylabel '{/*1.5 Scalability}';
set output 'threadscalability.png'
plot 'threadplotdata.dat' u 1:(oneworkerthreadtime/$2) title 'Thread version scalability - actual'\
w lp ls 1,\
x title 'Thread version scalability - ideal' ls 2

set output 'ffscalability.png'
plot 'ffplotdata.dat' u 1:(oneworkerfftime/$2) title 'FastFlow version scalability - actual'\
w lp ls 1,\
x title 'FastFlow version scalability - ideal' ls 2


set ylabel '{/*1.5 Efficiency}'
set yrange [0:1.2]

set output 'threadefficiency.png'
plot 'threadplotdata.dat'u 1:((oneworkerthreadtime/$2)/$1) title 'Thread version efficiency  - actual'\
w lp ls 1, 1 title 'Thread version efficiency - ideal' ls 2

set output 'ffefficiency.png'
plot 'ffplotdata.dat'u 1:((oneworkerfftime/$2)/$1) title 'FastFlow version efficiency  - actual'\
w lp ls 1, 1 title 'FastFlow version efficiency - ideal' ls 2
