/*
 * Parallel Montecarlo integration implementation
 *
 * Copyright (C) 2018-2021, cosimone64
 *
 * This program is free software, released under the GNU GPLv3
 * (General Public License).
 *
 * You should have received a copy of the GNU GPLv3 with this software,
 * if not, please visit <https://www.gnu.org/licenses/gpl.html>
 */
#ifndef TYPES_GUARD
#define TYPES_GUARD

#include <memory>
#include <utility>
#include <queue>
#include <mutex>
#include <condition_variable>

/*
 * POD data types. These are the objects passed among pipeline stages.
 */
struct Task
{
	long double (*f)(long double);
	std::unique_ptr<std::vector<long double>> samples;
	const long double a;
	const long double b;
	const bool end_flag;

	Task(long double (*f)(long double),
	     std::unique_ptr<std::vector<long double>> &&samples,
	     long double a, long double b);
	Task();
};

struct MapToken
{
	long double (*f)(long double);
	std::unique_ptr<std::vector<long double>> samples;
	const long double interval_length;
	const bool end_flag;

	MapToken(long double (*f)(long double),
	         std::unique_ptr<std::vector<long double>> &&samples,
	         long double interval_length, bool flag);
};

struct ReduceToken
{
	std::unique_ptr<std::vector<long double>> samples;
	const long double interval_length;
	const bool end_flag;

	ReduceToken(std::unique_ptr<std::vector<long double>> &&samples,
	            long double interval_length,
	            bool flag);
};

struct ResultPrinterToken
{
	const long double value;
	const long double interval_length;
	const unsigned long number_of_samples;
	const bool end_flag;

	ResultPrinterToken(long double value, long double interval_length,
	                   unsigned long n, bool flag);
};

/*
 * This class template defines a thread-safe queue to be used for
 * communication. In order to avoid copying, the queue
 * operates entirely through unique_ptrs.
 */
template <typename T>
class SynchronizedQueue
{
	const unsigned async_degree;
	std::queue<std::unique_ptr<T>> q;
	std::mutex m;
	std::condition_variable cv;
public:
	SynchronizedQueue(const unsigned async_degree)
		: async_degree {async_degree}, q {}, m {}, cv {} {}

	std::unique_ptr<T>
	get()
	{
		std::unique_lock<std::mutex> lk {m};
		while (q.empty())
			cv.wait(lk);
		std::unique_ptr<T> element {std::move(q.front())};
		q.pop();
		cv.notify_one();
		return element;
	}
	void put(std::unique_ptr<T> &&element)
	{
		std::unique_lock<std::mutex> lk {m};
		while (q.size() >= async_degree)
			cv.wait(lk);
		q.push(std::move(element));
		cv.notify_one();
	}
};
#endif
