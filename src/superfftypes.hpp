/*
 * Parallel Montecarlo integration implementation
 *
 * Copyright (C) 2018-2021, cosimone64
 *
 * This program is free software, released under the GNU GPLv3
 * (General Public License).
 *
 * You should have received a copy of the GNU GPLv3 with this software,
 * if not, please visit <https://www.gnu.org/licenses/gpl.html>
 */
#ifndef SUPERFF_TYPES_GUARD
#define SUPERFF_TYPES_GUARD

#include <cstdint>

template<typename FloatType>
struct Task
{
	FloatType (*f)(FloatType);
	FloatType a;
	FloatType b;
	std::uint_fast32_t number_of_samples;
	std::uint_fast32_t id;
};
/*
 * Workers only need to output their partial result,
 * the interval length and the number of samples to
 * the Gatherer. This is enough for the latter to
 * assemble the complete result. The id is the same
 * as the corresponding received Task from the emitter.
 */
template<typename AccumulatorType, typename FloatType>
struct WorkerOutputToken
{
	AccumulatorType partial_result_value;
	FloatType interval_length;
	std::uint_fast32_t number_of_samples;
	std::uint_fast32_t id;
};
#endif
