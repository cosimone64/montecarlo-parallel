/*
 * Parallel Montecarlo integration implementation
 *
 * Copyright (C) 2018-2021, cosimone64
 *
 * This program is free software, released under the GNU GPLv3
 * (General Public License).
 *
 * You should have received a copy of the GNU GPLv3 with this software,
 * if not, please visit <https://www.gnu.org/licenses/gpl.html>
 */
#include <iostream>
#include <fstream>
#include <streambuf>
#include <vector>
#include <string>
#include <thread>
#include <atomic>
#include <random>
#include <cstdint>
#include <cmath>
#include <unistd.h>

#ifdef TEST
#include <chrono>
#endif

#include "commonops.hpp"

/*
 * Each input task is packed inside this structure.
 * The FloatType template parameter allows choosing
 * precision to improve performance/accuracy.
 */
template<typename FloatType>
struct Task
{
	FloatType (*f)(FloatType);
	FloatType a;
	FloatType b;
	std::uint_fast32_t number_of_samples;
	bool end_flag;
};

/*
 * Parses the function text token and returns a function
 * pointer to use the required function.
 */
template<typename FloatType>
FloatType
(*get_function(const std::string &name))(FloatType)
{
	static constexpr char FUNCTION_ERROR[] {
		"Error: specified function does not exist"};
	if (name == "square") {
		return [](FloatType x) { return x * x; };
	} else if (name == "cube") {
		return [](FloatType x) { return x * x * x; };
	} else if (name == "sin") {
		return std::sin;
	} else {
		std::cerr << FUNCTION_ERROR << std::endl;
		exit(EXIT_FAILURE);
	}
	return nullptr;
}
/*
 * Returns a random number representing the number of samples
 * to use to compute the integral.
 */
inline std::uint_fast32_t
get_number_of_samples()
{
	static constexpr std::uint_fast32_t MAX_N {1000000ul};
#ifndef TEST
	static std::uint_fast16_t MIN_N {512ul};
	static std::random_device rd;
	static std::mt19937 rng {rd()};
	static std::uniform_int_distribution<std::uint_fast32_t> distr {
		MIN_N, MAX_N};
	return (std::uint_fast32_t) distr(rng);
#else
	return MAX_N;
#endif
}
/*
 * Returns the next task from the input file. The filename
 * is optional, tasks will be retrieved from the last used
 * filename.
 */
template<typename FloatType>
inline Task<FloatType>
get_task(const std::string &filename="")
{
	static constexpr char INTERVAL_ERROR[] {"Error: first interval "
		"extreme (a) is larger than the second one (b) in line: "};
	constexpr Task<FloatType> last_task {std::sin, 0, 1, 1, true};
	static std::ifstream file {filename};
	static std::uint_fast32_t line_num {0};
	std::string function_name;
	FloatType a;
	FloatType b;

	if (file >> function_name >> a >> b) {
		++line_num;
		if (a > b) {
			std::cerr << INTERVAL_ERROR << line_num << std::endl;
			exit(EXIT_FAILURE);
		}
		const std::uint_fast32_t sample_num {get_number_of_samples()};
		FloatType (*func)(FloatType) {get_function<FloatType>(function_name)};
		return Task<FloatType> {func, a, b, sample_num, false};
	}
	return last_task;
}
/*
 * Returns true if every flag in the vector is true, false
 * if at least one is false.
 */
inline bool
all_marked(const std::vector<std::atomic_bool> &flags)
{
	for (const auto &flag : flags) {
		if (!flag.load())
			return false;
	}
	return true;
}
/*
 * Worker function. Reads parameters in the current task
 * and computes the local result, which is obtained by
 * summing f(x) over all the local samples. Random samples
 * are computed "on the fly" without storing them in an
 * array first, they are simply added to the local result
 * once f(x) is computed.
 */
template<typename AccumulatorType, typename FloatType>
inline AccumulatorType
compute_partial_result(const Task<FloatType> &current_task,
                       const std::uint_fast16_t workernum,
                       const std::uint_fast16_t worker_seed,
                       const std::uint_fast16_t thread_id=0)
{
	thread_local std::random_device rd;
	thread_local std::mt19937 eng {rd()};

	std::uniform_real_distribution<FloatType> distr {current_task.a, current_task.b};
	const std::uint_fast32_t partition_size {current_task.number_of_samples / workernum};
	const std::uint_fast32_t start_index {partition_size * thread_id};
	const std::uint_fast32_t end_index {
		thread_id == workernum - 1 ?
			current_task.number_of_samples : partition_size * (thread_id + 1)};

	AccumulatorType local_result {0};
	for (std::uint_fast32_t i {start_index}; i < end_index; ++i) {
		auto x {current_task.f(distr(eng))};
		std::cout << "Thread id: " << thread_id << " and number: "
			<< x << std::endl;
		local_result += x;
	}
	return local_result;
}
template<typename AccumulatorType, typename FloatType>
inline void
work(const std::uint_fast32_t workernum,
     const std::uint_fast32_t thread_id,
     const Task<FloatType> &current_task,
     AccumulatorType &local_result,
     std::atomic_bool &flag)
{
	while (!current_task.end_flag) {
		while (flag.load())
			;
		local_result = compute_partial_result<AccumulatorType, FloatType>(
				current_task, workernum, thread_id);
		flag.store(true);
	}
}
inline std::ostream
get_output_stream(const std::string &outputfilename)
{
	const std::ofstream fileoutputstream {outputfilename};
	std::streambuf *buf {outputfilename != "" ?
		fileoutputstream.rdbuf() : std::cout.rdbuf()};
	return std::ostream {buf};
}
/*
 * Starts a number of additional workers equal to workernum - 1
 * and returns a std::vector containing them.
 */
template<typename AccumulatorType, typename FloatType>
inline std::vector<std::thread>
start_workers(const std::uint_fast16_t workernum,
              std::vector<AccumulatorType> &partial_results,
              std::vector<std::atomic_bool> &worker_flags,
              Task<FloatType> &task)
{
	std::vector<std::uint_fast16_t> worker_seeds {get_worker_seeds(workernum)};
	std::vector<std::thread> worker_threads;
	/*
	 * Indices start at 1 since worker 0 is the master.
	 */
	for (std::uint_fast16_t thread_id {1}; thread_id < workernum; ++thread_id) {
		cpu_set_t cpuset;
		CPU_ZERO(&cpuset);
		CPU_SET(thread_id, &cpuset);
		worker_threads.push_back(std::thread {work<AccumulatorType, FloatType>,
				workernum,
				thread_id,
				worker_seeds[thread_id],
				std::ref(task),
				std::ref(partial_results[thread_id]),
				std::ref(worker_flags[thread_id - 1])});
		if (pthread_setaffinity_np(worker_threads[thread_id - 1].native_handle(),
					sizeof(cpu_set_t),
					&cpuset)) {
			std::cerr << "Error while setting affinity on CPU "
				<< thread_id << std::endl;
			std::exit(EXIT_FAILURE);
		}
	}
	return worker_threads;
}
/*
 * Computes the full result from a set of partial results and the remaining
 * parameters of the current task, and then prints it to output.
 */
template<typename AccumulatorType, typename FloatType>
inline void
print_full_result(const std::vector<AccumulatorType> &partial_results,
                  const Task<FloatType> &task,
                  std::ostream &output_stream)
{
#ifndef TEST
	constexpr char separator {'\n'};
#endif
	AccumulatorType accumulated_result {0};
	for (const auto &partial_result : partial_results)
		accumulated_result += partial_result;
	const FloatType interval_length {task.b - task.a};
	const AccumulatorType final_result {
		(accumulated_result * interval_length)
			/ task.number_of_samples};
#ifndef TEST
	output_stream << final_result << separator;
#endif
}
int
main(int argc, char *argv[])
{
#ifdef TEST
	auto start_time = std::chrono::steady_clock::now();
#endif
	std::uint_fast16_t workernum {1};
	std::string inputfilename {""};
	std::string outputfilename {""};
	parse_args(argc, argv, workernum, inputfilename, outputfilename);
	std::ostream output_stream {get_output_stream(outputfilename)};

	std::vector<std::atomic_bool> worker_flags (workernum - 1);
	for (auto &flag : worker_flags)
		flag.store(false);

	std::vector<double> partial_results (workernum);
	double &master_partial_result {partial_results[0]};

	Task<float> task {get_task<float>(inputfilename)};
	std::vector<std::thread> worker_threads {
		start_workers(workernum, partial_results, worker_flags, task)};
	for (; !task.end_flag; task = get_task<float>()) {
		for (auto &flag : worker_flags)
			flag.store(false);
		master_partial_result = compute_partial_result<double, float>(task, workernum);
		while (!all_marked(worker_flags))
			;
		print_full_result(partial_results, task, output_stream);
	}
	for (auto &flag : worker_flags)
		flag.store(false);
	for (auto &t : worker_threads)
		if (t.joinable())
			t.join();
#ifdef TEST
	const auto current_time {std::chrono::steady_clock::now()};
	std::chrono::microseconds delta_time {
		std::chrono::duration_cast<std::chrono::microseconds>(
				current_time - start_time)};
	std::cout << "Time in microseconds: "
		<< delta_time.count() << std::endl;
#endif
	return 0;
}
