#!/bin/env sh
# Long string is not wrapped to be absolutely sure that the correct output is produced.

threadversion='./superthreaded'
ffversion='./superff'
sequentialversion='./tweakedsequentialmontecarlo'
input_file=$1

test_version()
{
	echo "Testing $1 with a single worker"
	$1 -d 1 -i $2
	printf "\n"

	for degree in `eval echo "{$3..250..$3}"`; do
		echo "Testing $1 with $degree workers"
		COMMAND="$1 -d $degree -i $2"
		$COMMAND
		printf "\n"
	done
}

echo "Testing sequential version"
$sequentialversion $1
printf "\n"
test_version $threadversion $input_file 2
test_version $ffversion $input_file 2
