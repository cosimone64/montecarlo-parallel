/*
 * Parallel Montecarlo integration implementation
 *
 * Copyright (C) 2018-2021, cosimone64
 *
 * This program is free software, released under the GNU GPLv3
 * (General Public License).
 *
 * You should have received a copy of the GNU GPLv3 with this software,
 * if not, please visit <https://www.gnu.org/licenses/gpl.html>
 */
#include <iostream>
#include <fstream>
#include <streambuf>
#include <string>
#include <memory>
#include <utility>
#include <random>
#include <ff/node.hpp>
#include <ff/parallel_for.hpp>
#include <ff/map.hpp>

#include "ffmodules.hpp"

static constexpr auto INTERVAL_ERROR =
	"Error: first interval extreme (a) is larger than "
	"the second one (b) in line: ";

static constexpr auto FUNCTION_ERROR =
	"Error: specified function does not exist";

/*
 * Constructors for POD data types.
 */
SamplerTask::SamplerTask(long double (*f)(long double),
                         std::unique_ptr<std::vector<long double>> &&samples,
                         const long double a,
                         const long double b)
	: f {f}, samples {std::move(samples)},
	  a {a}, b {b} {}

MapperTask::MapperTask(long double (*f)(long double),
                       std::unique_ptr<std::vector<long double>> &&samples,
                       const long double interval_length)
	: f {f}, samples {std::move(samples)},
	  interval_length {interval_length} {}

ReducerTask::ReducerTask(std::unique_ptr<std::vector<long double>> &&samples,
                         const long double interval_length)
	: samples {std::move(samples)}, interval_length {interval_length} {}

/*
 * Stream generating functions
 */
long double
(*StreamGenerator::get_function(const std::string &name))(long double)
{
	if (name == "square") {
		return [](long double x) { return x * x; };
	} else if (name == "cube") {
		return [](long double x) { return x * x * x; };
	} else if (name == "sin") {
		return std::sin;
	} else {
		std::cerr << FUNCTION_ERROR << std::endl;
		exit(EXIT_FAILURE);
	}
	return nullptr;
}
unsigned long StreamGenerator::get_number_of_samples(const unsigned min_n) {
	constexpr unsigned long MAX_N {1000000ul};
#ifndef TEST
	static std::random_device rd;
	static std::mt19937 rng {rd()};
	std::uniform_int_distribution<unsigned long> distr {min_n, MAX_N};
	return (unsigned long) distr(rng);
#else
	return MAX_N;
#endif
}
StreamGenerator::StreamGenerator(const std::string &filename,
                                 const unsigned reducer_degree)
	: filename {filename},
	min_number_of_samples {reducer_degree * reducer_degree} {}

SamplerTask *
StreamGenerator::svc(SamplerTask *)
{
	std::ifstream file {filename};
	std::string function_name;
	unsigned line_num {0};
	long double a;
	long double b;

	while (file >> function_name >> a >> b) {
		line_num++;
		if (a > b) {
			std::cerr << INTERVAL_ERROR << line_num << std::endl;
			exit(EXIT_FAILURE);
		}
		const unsigned long samples_num {get_number_of_samples(min_number_of_samples)};
		SamplerTask *output {new SamplerTask
			{get_function(function_name),
				std::make_unique<std::vector<long double>>(samples_num),
				a, b}};
		ff_send_out(output);
	}
	return EOS;
}

/*
 * Sampling functions
 */
Sampler::Sampler(const unsigned degree) : degree {degree} {}

MapperTask *
Sampler::svc(SamplerTask *input)
{
	std::uniform_real_distribution<long double> distr {
		input->a, input->b};
	std::vector<long double> &samples {*(input->samples)};
	parallel_for_static(0, samples.size(), 1, 0, [&](const unsigned long &i)
			{ thread_local static std::random_device rd;
			thread_local static std::mt19937 rng {rd()};
			samples[i] = distr(rng); },
			degree);
	const long double interval_length {input->b - input->a};
	auto output {new MapperTask {input->f,
		std::move(input->samples), interval_length}};
	delete input;
	return output;
}
/*
 * Mapping functions
 */
Mapper::Mapper(const unsigned degree) : degree {degree} {}

ReducerTask *
Mapper::svc(MapperTask *input)
{
	long double (*f)(long double) {input->f};
	std::vector<long double> &samples = *input->samples;

	parallel_for_static(0, samples.size(), 1, 0,
			[&](const unsigned long &i){
			samples[i] = f(samples[i]); }, degree);
	auto output {new ReducerTask {std::move(input->samples),
		input->interval_length}};
	delete input;
	return output;
}
/*
 * Reducing functions
 */
Reducer::Reducer(const unsigned degree) : degree {degree}, pfr{degree} {}

ResultPrinterTask *
Reducer::svc(ReducerTask *input)
{
	const std::vector<long double> &samples = *input->samples;
	auto reduce_f = [](long double &sum,
			const long double &x) { sum += x; };
	auto body_f = [&](const unsigned long &i,
			long double &sum) { sum += samples[i]; };
	long double sum {0};

	pfr.parallel_reduce(sum, 0.0, 0l, samples.size(), body_f, reduce_f, degree);
	auto output {new ResultPrinterTask {sum,
		input->interval_length, input->samples->size()}};
	delete input;
	return output;
}

/*
 * Final result printing functions
 */
long double *
ResultPrinter::svc(ResultPrinterTask *input)
{
	const long double &length {input->interval_length};
	output_stream << input->partial_result * length / input->n << separator;
	delete input;
	return GO_ON;
}
ResultPrinter::ResultPrinter(const std::string &outputfilename)
	: separator {'\n'},
	  fileoutputstream {outputfilename},
	  output_stream {outputfilename != "" ?
	                 fileoutputstream.rdbuf() : std::cout.rdbuf()} {}
