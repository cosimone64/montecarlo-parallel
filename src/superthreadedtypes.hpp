/*
 * Parallel Montecarlo integration implementation
 *
 * Copyright (C) 2018-2021, cosimone64
 *
 * This program is free software, released under the GNU GPLv3
 * (General Public License).
 *
 * You should have received a copy of the GNU GPLv3 with this software,
 * if not, please visit <https://www.gnu.org/licenses/gpl.html>
 */
#ifndef SUPERTHREADED_TYPES_GUARD
#define SUPERTHREADED_TYPES_GUARD

#include <array>
#include <atomic>
#include <cstdint>

/*
 * Each input task is packed inside this structure.
 * The FloatType template parameter allows choosing
 * precision to improve performance/accuracy.
 */
template<typename FloatType>
struct Task
{
	FloatType (*f)(FloatType);
	FloatType a;
	FloatType b;
	std::uint_fast32_t number_of_samples;
	std::uint_fast32_t id;
	bool end_flag;
};
/*
 * Workers only need to output their partial result,
 * the interval length and the number of samples to
 * the Gatherer. This is enough for the latter to
 * assemble the complete result. The id is the same
 * as the corresponding received Task from the emitter.
 *
 * The additional template parameter AccumulatorType allows to
 * choose an appropriate floating point type with greater precision
 * to accumulate results. Using the same type may cause overflow.
 */
template<typename AccumulatorType, typename FloatType>
struct WorkerOutputToken
{
	AccumulatorType partial_result_value;
	FloatType interval_length;
	std::uint_fast32_t number_of_samples;
	std::uint_fast32_t id;
	bool end_flag;
};
/*
 * Thread-safe queue to be used for In order to avoid copying, the queue
 * relies on move semantics, by explicitly accepting and returning
 * rvalue references only.
 *
 * The queue is implemented via a circular array, without using
 * locks. Synchronization relies on atomic operations on the indices.
 */
template <typename T, typename UIntType = std::uint_fast8_t,
	 UIntType Size = std::numeric_limits<UIntType>::max()>
class McQueue
{
	std::array<T, Size> queue_buffer;
	std::atomic<UIntType> head;
	std::atomic<UIntType> tail;

	inline UIntType
	get_next_index(const std::atomic<UIntType> &index)
	{
		return (index.load() + 1) % Size;
	}
public:
	McQueue() : head {UIntType {0}}, tail {UIntType {0}} {}
	
	inline T
	get()
	{
		const auto current_head = head.load();
		while (current_head == tail.load())
			;
		const auto element = std::move(queue_buffer[current_head]);
		head.store(get_next_index(head));
		return element;
	}
	inline void
	put(T &&element)
	{
		const auto current_tail = tail.load();
		const auto next_tail = get_next_index(tail);
		while (next_tail == head.load())
			;
		queue_buffer[current_tail] = std::move(element);
		tail.store(next_tail);
	}
};
#endif
