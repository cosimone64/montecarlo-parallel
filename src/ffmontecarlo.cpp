/*
 * Parallel Montecarlo integration implementation
 *
 * Copyright (C) 2018-2021, cosimone64
 *
 * This program is free software, released under the GNU GPLv3
 * (General Public License).
 *
 * You should have received a copy of the GNU GPLv3 with this software,
 * if not, please visit <https://www.gnu.org/licenses/gpl.html>
 */
#include <iostream>
#include <string>
#include <unordered_map>
#include <utility>
#include <random>
#include <ff/pipeline.hpp>
#include "ffmodules.hpp"

static constexpr auto PIPELINE_ERROR = "Error while running pipeline";

void
parse_args(const int &argc, char **&argv,
           std::unordered_map<char, unsigned> &degrees,
           std::string &inputfilename,
           std::string &outputfilename);

int
main(int argc, char *argv[])
{
	std::unordered_map<char, unsigned> degrees {{'s', 1},
		{'m', 1}, {'r', 1}};
	std::string inputfilename {""};
	std::string outputfilename {""};

	parse_args(argc, argv, degrees, inputfilename, outputfilename);
	StreamGenerator stream_generator {inputfilename, degrees['r']};
	Sampler sampler {degrees['s']};
	Mapper mapper {degrees['m']};
	Reducer reducer {degrees['r']};
	ResultPrinter result_printer {outputfilename};

	ff::ff_Pipe<> pipe {stream_generator, sampler, mapper,
		reducer, result_printer};
	if (pipe.run_and_wait_end())
		ff::error(PIPELINE_ERROR);
	return 0;
}

void
parse_args(const int &argc, char **&argv,
           std::unordered_map<char, unsigned> &degrees,
           std::string &inputfilename,
           std::string &outputfilename)
{
	bool error_found {false};
	if (argc < 2)
		error_found = true;
	int c;
	while ((c = getopt(argc, argv, "a:s:m:r:i:o:")) != -1) {
		if (c == 'a') {
			for (auto &v : degrees)
				v.second = std::atoi(optarg);
		} else if (c == 's' || c == 'm' || c == 'r') {
			degrees[c] = std::atoi(optarg);
		} else if (c == 'i') {
			inputfilename = optarg;
		} else if (c == 'o') {
			outputfilename = optarg;
		} else {
			error_found = true;
		}
	}
	if (degrees['s'] < 1 || degrees['m'] < 1 || degrees['r'] < 1) {
		std::cerr << "Error: number of workers must be at least 1" << std::endl;
		error_found = true;
	}
	if (inputfilename == "") {
		std::cerr << "Error, no input file specified" << std::endl;
		error_found = true;
	}
	if (error_found) {
		std::cerr << "Use as " << argv[0]
			<< " <degrees> -i <inputfilename> [-o <outputfilename>]"
			<< std::endl;
		exit(EXIT_FAILURE);
	}
}
