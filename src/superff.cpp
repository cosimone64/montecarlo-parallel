/*
 * Parallel Montecarlo integration implementation
 *
 * Copyright (C) 2018-2021, cosimone64
 *
 * This program is free software, released under the GNU GPLv3
 * (General Public License).
 *
 * You should have received a copy of the GNU GPLv3 with this software,
 * if not, please visit <https://www.gnu.org/licenses/gpl.html>
 */
#ifdef TEST
#include <chrono>
#include <iostream>
#endif
#include <cstdint>
#include <ff/ff.hpp>
#include <ff/node.hpp>
#include <ff/farm.hpp>
#include <memory>
#include <string>
#include <utility>

#include "superfftypes.hpp"
#include "superffmodules.hpp"
#include "commonops.hpp"

using std::make_unique;
using std::move;
#ifdef TEST
using std::chrono::microseconds;
using std::chrono::duration_cast;
using std::chrono::steady_clock;
#endif

int
main(int argc, char *argv[])
{
#ifdef TEST
	auto start_time = steady_clock::now();
#endif
	std::uint_fast16_t workernum {1};
	std::string inputfilename {""};
	std::string outputfilename {""};
	parse_args(argc, argv, workernum, inputfilename, outputfilename);
	
	auto scatterer = make_unique<Scatterer<float>>(workernum, inputfilename);
	auto gatherer  = make_unique<
		Gatherer<double, float>>(workernum, outputfilename);
	auto workers = std::vector<std::unique_ptr<ff::ff_node>> {};
	auto worker_seeds = get_worker_seeds(workernum);
	for (std::uint_fast16_t i {0}; i < workernum; ++i)
		workers.emplace_back(make_unique<Worker<double, float>>(
					worker_seeds[i]));
	ff::ff_Farm<Task<float>, double> map_farm {move(workers),
		move(scatterer), move(gatherer)};
	if (map_farm.run_and_wait_end())
		ff::error("Error while running map");
#ifdef TEST
	const auto current_time = steady_clock::now();
	auto delta_time = duration_cast<microseconds>(current_time - start_time);
	std::cout << "Time in microseconds: "
		<< delta_time.count() << std::endl;
#endif
	return 0;
}
