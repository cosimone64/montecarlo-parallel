/*
 * Parallel Montecarlo integration implementation
 *
 * Copyright (C) 2018-2021, cosimone64
 *
 * This program is free software, released under the GNU GPLv3
 * (General Public License).
 *
 * You should have received a copy of the GNU GPLv3 with this software,
 * if not, please visit <https://www.gnu.org/licenses/gpl.html>
 */
#ifndef PARALLELTEMPLATE_GUARD
#define PARALLELTEMPLATE_GUARD

#include <atomic>
#include <thread>
#include <vector>
#include <memory>

#include "threadtypes.hpp"

template<typename InputType, typename OutputType>
class ParallelStage
{
	SynchronizedQueue<InputType> &input_queue;
	SynchronizedQueue<OutputType> &output_queue;
	std::vector<std::thread> workers;
	std::vector<std::atomic_bool> master_to_worker_flags;
	std::vector<std::atomic_bool> worker_to_master_flags;
	std::unique_ptr<InputType> current_input_token;
	const unsigned workernum;

	inline bool
	all_marked(const std::vector<std::atomic_bool> &flags);

	inline void
	master();

	inline void
	work(unsigned thread_id,
	     std::atomic_bool &master_to_worker_flag,
	     std::atomic_bool &worker_to_master_flag);

	inline void
	update_buffer(const unsigned long start_index,
	              const unsigned long end_index,
	              const InputType &current_input_token);

	inline std::unique_ptr<OutputType>
	get_output_token(InputType &current_input_token);
public:
	ParallelStage(const unsigned workernum,
	              SynchronizedQueue<InputType> &input_queue,
	              SynchronizedQueue<OutputType> &output_queue);

	void
	run();
};
/*
 * These typedefs are used to emulate inherited classes with templates,
 * avoiding the cost of virtual functions.
 */
typedef ParallelStage<Task, MapToken> Sampler;
typedef ParallelStage<MapToken, ReduceToken> Mapper;
typedef ParallelStage<ReduceToken, ResultPrinterToken> Reducer;

template<typename InputType, typename OutputType>
void
ParallelStage<InputType, OutputType>::run()
{
	for (unsigned i {0}; i < workernum; ++i) {
		workers.push_back(std::thread {
				&ParallelStage<InputType, OutputType>::work,
				this, i, std::ref(master_to_worker_flags[i]),
				std::ref(worker_to_master_flags[i])});
	}
	master();
	for (auto &t : workers) {
		if (t.joinable())
			t.join();
	}
}
template<typename InputType, typename OutputType>
void
ParallelStage<InputType, OutputType>::master()
{
	bool end_flag {false};
	while (!end_flag) {
		for (auto &flag : worker_to_master_flags)
			flag.store(false);
		current_input_token = std::move(input_queue.get());
		for (auto &flag : master_to_worker_flags)
			flag.store(true);
		while (!all_marked(worker_to_master_flags))
			;
		std::unique_ptr<OutputType> output_token {
			get_output_token(*current_input_token)};
		output_queue.put(std::move(output_token));
		end_flag = current_input_token->end_flag;
	}
}

template<typename InputType, typename OutputType>
bool
ParallelStage<InputType, OutputType>::all_marked(const std::vector<std::atomic_bool> &flags)
{
	for (const auto &flag : flags) {
		if (!flag.load())
			return false;
	}
	return true;
}


template<typename InputType, typename OutputType>
void
ParallelStage<InputType, OutputType>::work(unsigned thread_id,
                                           std::atomic_bool &master_to_worker_flag,
                                           std::atomic_bool &worker_to_master_flag)
{
	bool end_flag {false};
	while (!end_flag) {
		if (!master_to_worker_flag.load())
			continue;
		master_to_worker_flag.store(false);
		const unsigned long partition_size {
			current_input_token->samples->size() / workernum};
		const unsigned long start_index {partition_size * thread_id};
		const unsigned long end_index {
			thread_id == workernum - 1 ?
				current_input_token->samples->size()
				: partition_size * (thread_id + 1)};
		update_buffer(start_index, end_index,
				*current_input_token);
		end_flag = current_input_token->end_flag;
		worker_to_master_flag.store(true);
	}
}

template<typename InputType, typename OutputType>
ParallelStage<InputType, OutputType>::ParallelStage(const unsigned workernum,
                                                    SynchronizedQueue<InputType> &input_queue,
                                                    SynchronizedQueue<OutputType> &output_queue)
	: input_queue {input_queue}, output_queue {output_queue},
	  master_to_worker_flags (workernum),
	  worker_to_master_flags (workernum),
	  current_input_token {nullptr},
	  workernum {workernum}
{
	for (auto &flag : master_to_worker_flags)
		flag.store(false);
	for (auto &flag : worker_to_master_flags)
		flag.store(true);
}

#endif
