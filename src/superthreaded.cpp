/*
 * Parallel Montecarlo integration implementation
 *
 * Copyright (C) 2018-2021, cosimone64
 *
 * This program is free software, released under the GNU GPLv3
 * (General Public License).
 *
 * You should have received a copy of the GNU GPLv3 with this software,
 * if not, please visit <https://www.gnu.org/licenses/gpl.html>
 */
#include <cstdint>
#ifdef TEST
#include <chrono>
#endif
#include <string>
#include <thread>
#include <vector>

#include "superthreadedtypes.hpp"
#include "superthreadedmodules.hpp"
#include "commonops.hpp"

using std::chrono::microseconds;
using std::chrono::duration_cast;

int
main(int argc, char *argv[])
{
#ifdef TEST
	auto start_time = std::chrono::steady_clock::now();
#endif
	std::uint_fast16_t workernum {1};
	std::string inputfilename  {""};
	std::string outputfilename {""};
	parse_args(argc, argv, workernum, inputfilename, outputfilename);
	const std::uint_fast16_t scatterer_id {workernum};
	const std::uint_fast16_t gatherer_id  {workernum + 1};
	std::vector<McQueue<Task<float>>> scatterer_worker_queues (workernum);
	std::vector<McQueue<
		WorkerOutputToken<double, float>>> worker_gatherer_queues (workernum);

	auto scatterer_thread = start_scatterer(scatterer_id, inputfilename,
			scatterer_worker_queues);
	auto gatherer_thread = start_gatherer(gatherer_id, outputfilename,
			worker_gatherer_queues);
	auto worker_threads = start_workers<double, float>(workernum,
			scatterer_worker_queues, worker_gatherer_queues);
	if (scatterer_thread.joinable())
		scatterer_thread.join();
	for (auto &t : worker_threads) {
		if (t.joinable()) {
			t.join();
		}
	}
	if (gatherer_thread.joinable())
		gatherer_thread.join();
#ifdef TEST
	const auto current_time = std::chrono::steady_clock::now();
	auto delta_time = duration_cast<microseconds>(current_time - start_time);
	std::cout << "Time in microseconds: "
		<< delta_time.count() << std::endl;
#endif
	return 0;
}
