/*
 * Parallel Montecarlo integration implementation
 *
 * Copyright (C) 2018-2021, cosimone64
 *
 * This program is free software, released under the GNU GPLv3
 * (General Public License).
 *
 * You should have received a copy of the GNU GPLv3 with this software,
 * if not, please visit <https://www.gnu.org/licenses/gpl.html>
 */
#ifndef COMMONOPS_GUARD
#define COMMONOPS_GUARD

#include <cstdint>
#include <string>
#include <unistd.h>

/*
 * NB: Inline functions must have the full definition visible
 * at compile time, therefore they too are placed in a header file.
 */

/*
 * Parses input arguments using getopt().
 * 	-d: parallelism degree of the application (optional)
 * 	-i: input file
 * 	-o: output file (optional)
 *
 * 	All arguments are stored in the corresponding
 * 	output parameters.
 */
inline void
parse_args(const int &argc, char **&argv, std::uint_fast16_t &workernum,
           std::string &inputfilename, std::string &outputfilename)
{
	auto error_found = false;
	if (argc < 2)
		error_found = true;
	auto c = std::int_fast16_t {};
	while ((c = getopt(argc, argv, "d:i:o:")) != -1) {
		if (c == 'd')
			workernum = std::atoi(optarg);
		else if (c == 'i')
			inputfilename = optarg;
		else if (c == 'o')
			outputfilename = optarg;
		else
			error_found = true;
	}
	if (workernum < 1) {
		std::cerr << "Error: number of workers must be at least 1" << std::endl;
		error_found = true;
	}
	if (inputfilename == "") {
		std::cerr << "Error, no input file specified" << std::endl;
		error_found = true;
	}
	if (error_found) {
		std::cerr << "Use as " << argv[0]
			<< " -d <degrees> -i <inputfilename> [-o <outputfilename>]"
			<< std::endl;
		exit(EXIT_FAILURE);
	}
}

template<typename SeedType = std::uint_fast16_t>
inline std::vector<SeedType>
get_worker_seeds(const std::uint_fast16_t workernum)
{
	std::random_device rd;
	std::mt19937 engine;
	std::vector<SeedType> seed_vector (workernum);
	for (auto &seed : seed_vector)
		seed = engine();
	return seed_vector;
}
#endif
