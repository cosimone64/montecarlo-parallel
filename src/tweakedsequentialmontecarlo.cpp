/*
 * Parallel Montecarlo integration implementation
 *
 * Copyright (C) 2018-2021, cosimone64
 *
 * This program is free software, released under the GNU GPLv3
 * (General Public License).
 *
 * You should have received a copy of the GNU GPLv3 with this software,
 * if not, please visit <https://www.gnu.org/licenses/gpl.html>
 */
#include <iostream>
#include <fstream>
#include <random>
#include <cmath>

#ifdef TEST
#include <chrono>
#endif

template<typename FloatType>
struct SequentialTask
{
	FloatType (*f)(FloatType);
	FloatType a;
	FloatType b;
	unsigned long number_of_samples;
	bool end_flag;
};

template<typename FloatType>
FloatType
(*get_function(const std::string &name))(FloatType)
{
	static constexpr char FUNCTION_ERROR[] {
		"Error: specified function does not exist"};
	if (name == "square") {
		return [](FloatType x) { return x * x; };
	} else if (name == "cube") {
		return [](FloatType x) { return x * x * x; };
	} else if (name == "sin") {
		return std::sin;
	} else {
		std::cerr << FUNCTION_ERROR << std::endl;
		exit(EXIT_FAILURE);
	}
	return nullptr;
}
unsigned long
get_number_of_samples()
{
	static constexpr unsigned long MAX_N {1000000ul};
#ifndef TEST
	static constexpr unsigned MIN_N {512ul};
	static std::random_device rd;
	static std::mt19937 rng {rd()};
	static std::uniform_int_distribution<unsigned long> distr {
		MIN_N, MAX_N};
	return (unsigned long) distr(rng);
#else
	return MAX_N;
#endif
}
template<typename FloatType>
SequentialTask<FloatType>
get_task(const char *filename="")
{
	static constexpr char INTERVAL_ERROR[] {"Error: first interval "
		"extreme (a) is larger than the second one (b) in line: "};
	static std::random_device rd;
	static std::mt19937 rng {rd()};
	static std::ifstream file {filename};
	std::string function_name;
	unsigned line_num {0};
	FloatType a, b;
	const SequentialTask<FloatType> last_task {std::sin, 0, 1, 1, true};

	if (file >> function_name >> a >> b) {
		line_num++;
		if (a > b) {
			std::cerr << INTERVAL_ERROR << line_num << std::endl;
			exit(EXIT_FAILURE);
		}
		return SequentialTask<FloatType> {get_function<FloatType>(function_name), a, b,
			get_number_of_samples(), false};
	}
	return last_task;
}
int
main(int argc, char *argv[])
{
	if (argc != 2) {
		std::cerr << "Use as " << argv[0]
			<< " <filename>" << std::endl;
		return -1;
	}
#ifdef TEST
	auto start_time {std::chrono::steady_clock::now()};
#endif
	static std::random_device rd;
	static std::mt19937 rng {rd()};

	for (SequentialTask<float> task {get_task<float>(argv[1])};
			!task.end_flag; task = get_task<float>()) {
		const unsigned long &n {task.number_of_samples};
		std::uniform_real_distribution<float> distr {task.a, task.b};

		double result {0};
		for (unsigned i {0}; i < n; ++i)
			result += task.f(distr(rng));
		const float interval_length {task.b - task.a};
		const double final_result {(result * interval_length) / n};
#ifndef TEST
		std::cout << final_result << '\n';
#endif
	}
#ifdef TEST
	const auto current_time {std::chrono::steady_clock::now()};
	std::chrono::microseconds delta_time {
		std::chrono::duration_cast<std::chrono::microseconds>(
				current_time - start_time)};
	std::cout << "Time in microseconds: "
		<< delta_time.count() << std::endl;
#endif
	return 0;
}
