/*
 * Parallel Montecarlo integration implementation
 *
 * Copyright (C) 2018-2021, cosimone64
 *
 * This program is free software, released under the GNU GPLv3
 * (General Public License).
 *
 * You should have received a copy of the GNU GPLv3 with this software,
 * if not, please visit <https://www.gnu.org/licenses/gpl.html>
 */
#ifndef SUPERFF_MODULES_GUARD
#define SUPERFF_MODULES_GUARD

#include <cstdint>
#include <ff/node.hpp>
#include <iostream>
#include <memory>
#include <random>
#include <string>
#include <unordered_map>
#include <utility>

#include "superfftypes.hpp"

/*
 * NB: Inline functions must have the full definition visible
 * at compile time, therefore they too are placed in a header file.
 */

/*
 * This class represents the Scatterer node: its job
 * is to mimic a data parallel computation by sending
 * the same task to each worker, which will then proceed
 * to compute the respective partitions.
 */
template<typename FloatType>
class Scatterer : public ff::ff_node_t<Task<FloatType>> {
	const std::uint_fast16_t workernum;
	std::ifstream       inputfile;

	inline auto get_function(const std::string &name) -> auto (*)(FloatType) -> FloatType;

	inline std::uint_fast16_t get_number_of_samples();
public:
	Scatterer(const std::uint_fast16_t workernum, const std::string &inputfilename)
		: workernum {workernum}, inputfile {inputfilename} {}

	Scatterer(const Scatterer &other) = delete;

	inline Task<FloatType> *
	svc(Task<FloatType> *);
};
/*
 * This class represents a single map worker.
 */
template<typename AccumulatorType, typename FloatType>
class Worker : public ff::ff_node_t<Task<FloatType>,
	WorkerOutputToken<AccumulatorType, FloatType>> // This is a template parameter!
{
	static std::uint_fast16_t workernum;
	const std::uint_fast16_t worker_id;
	std::mt19937 engine;
public:
	Worker(const uint_fast16_t worker_seed)
		: worker_id {workernum++}, engine {worker_seed} {}

	Worker(const Worker &other) = delete;

	inline WorkerOutputToken<AccumulatorType, FloatType> *
	svc(Task<FloatType> *task);
};
/*
 * This class represents the gatherer node: it receives
 * partial results from workers and builds complete results. In order
 * to distinguish partial results belonging to different stream tokens,
 * it checks task ids attached to each subtoken.
 */
template<typename AccumulatorType, typename FloatType>
class Gatherer
	: public ff::ff_node_t<WorkerOutputToken<AccumulatorType, FloatType>,
	                       AccumulatorType> {
	static constexpr auto separator = '\n';
	const std::uint_fast32_t workernum;
	std::unordered_map<std::uint_fast32_t, std::vector<AccumulatorType>> output_map;
	std::ostream output_stream;

	inline std::ostream
	get_output_stream(const std::string &outputfilename);
public:
	Gatherer(const std::uint_fast32_t workernum, const std::string &outputfilename)
		: workernum {workernum},
		  output_stream {get_output_stream(outputfilename)} {}

	Gatherer(const Gatherer &other) = delete;

	inline AccumulatorType *
	svc(WorkerOutputToken<AccumulatorType, FloatType>  *task);
};
/*
 * Parses the function text token and returns a function
 * pointer to use the required function.
 */
template<typename FloatType>
inline auto
Scatterer<FloatType>::get_function(const std::string &name) -> auto (*)(FloatType) -> FloatType
{
	static constexpr auto function_error =
		"Error: specified function does not exist";
	if (name == "square") {
		return [](FloatType x) { return x * x; };
	} else if (name == "cube") {
		return [](FloatType x) { return x * x * x; };
	} else if (name == "sin") {
		return std::sin;
	} else {
		std::cerr << function_error << std::endl;
		exit(EXIT_FAILURE);
	}
	return nullptr;
}
/*
 * Returns a random number representing the number of samples
 * to use to compute the integral.
 */
template<typename FloatType>
inline std::uint_fast32_t
Scatterer<FloatType>::get_number_of_samples()
{
	static constexpr uint_fast32_t MAX_N {1000000ul};
#ifndef TEST
	static uint_fast16_t MIN_N {512ul};
	static std::random_device rd;
	static std::mt19937 rng {rd()};
	static std::uniform_int_distribution<uint_fast32_t> distr {
		MIN_N, MAX_N};
	return static_cast<uint_fast32_t>(distr(rng));
#else
	return MAX_N;
#endif
}
/*
 * Returns the next task from the input file. The filename
 * is optional, tasks will be retrieved from the last used
 * filename.
 */
template<typename FloatType>
inline Task<FloatType> *
Scatterer<FloatType>::svc(Task<FloatType> *)
{
	std::uint_fast32_t line_num {0};
	std::string function_name;
	FloatType a;
	FloatType b;

	while (inputfile >> function_name >> a >> b) {
		++line_num;
		const auto sample_num = get_number_of_samples();
		auto func = get_function(function_name);
		for (uint_fast16_t i {0}; i < workernum; ++i) {
			auto *task = new Task<FloatType> {func, a, b, sample_num, line_num};
			this->ff_send_out(task); //Explicit "this" needed since it's a function template
		}
	}
	return static_cast<Task<FloatType> *>(this->EOS);
}
// Initializes static variable.
template<typename AccumulatorType, typename FloatType>
std::uint_fast16_t
Worker<AccumulatorType, FloatType>::workernum {0};

template<typename AccumulatorType, typename FloatType>
WorkerOutputToken<AccumulatorType, FloatType> *
Worker<AccumulatorType, FloatType>::svc(Task<FloatType> *task)
{
	const uint_fast32_t number_of_samples {task->number_of_samples};
	const uint_fast32_t partition_size {number_of_samples / workernum};
	const uint_fast32_t start_index {partition_size * worker_id};
	const uint_fast32_t end_index {worker_id == workernum - 1
			? number_of_samples
			: partition_size * (worker_id + 1)};
	std::uniform_real_distribution<FloatType> distr {task->a, task->b};

	AccumulatorType local_result {0};
	for (uint_fast32_t i {start_index}; i < end_index; ++i)
		local_result += task->f(distr(engine));
	const FloatType interval_length {task->b - task->a};
	const uint_fast32_t id {task->id};
	delete task;
	return new WorkerOutputToken<AccumulatorType, FloatType> {local_result,
		interval_length, number_of_samples, id};
}
/*
 * Returns the corresponding output stream object to write results to.
 * IMPORTANT: move semantics for streams are correcly implemented only
 * from the C++17 standard and beyond, so make sure to compile with a
 * C++17-compliant compiler!
 */
template<typename AccumulatorType, typename FloatType>
inline std::ostream
Gatherer<AccumulatorType, FloatType>::get_output_stream(const std::string &outputfilename)
{
	const std::ofstream fileoutputstream {outputfilename};
	std::streambuf *buf {outputfilename != "" ?
		fileoutputstream.rdbuf() : std::cout.rdbuf()};
	return std::ostream {buf};
}
/*
 * Main gatherer function. It stores partial results in a hashmap
 * mapping token ids to respective vectors of partial results.
 * When enough partial results have been received for a single
 * id, the respective full result is printed to output and the
 * corresponding vector deallocated.
 */
template<typename AccumulatorType, typename FloatType>
inline AccumulatorType *
Gatherer<AccumulatorType, FloatType>::svc(WorkerOutputToken<AccumulatorType,
                                          FloatType> *task)
{
	auto vector_pos = output_map.find(task->id);
	if (vector_pos == output_map.end()) {
		output_map[task->id] = std::vector<AccumulatorType> {};
		output_map[task->id].reserve(workernum);
		vector_pos = output_map.find(task->id);
	}
	auto &result_vector = vector_pos->second;
	result_vector.push_back(task->partial_result_value);

	if (result_vector.size() == workernum) {
		AccumulatorType accumulated_result {0};
		for (auto &x : result_vector)
			accumulated_result += x;
		const AccumulatorType final_result {
			(accumulated_result * task->interval_length)
				/ task->number_of_samples};
#ifndef TEST
		output_stream << final_result << separator;
#endif
		output_map.erase(vector_pos);
	}
	delete task;
	return static_cast<AccumulatorType *>(this->GO_ON);
}
#endif
