/*
 * Parallel Montecarlo integration implementation
 *
 * Copyright (C) 2018-2021, cosimone64
 *
 * This program is free software, released under the GNU GPLv3
 * (General Public License).
 *
 * You should have received a copy of the GNU GPLv3 with this software,
 * if not, please visit <https://www.gnu.org/licenses/gpl.html>
 */
#include <vector>
#include <memory>
#include <utility>
#include "threadtypes.hpp"

/*
 * Constructors for POD data types.
 */
Task::Task() : f {[](long double x) { return x * x; }},
               samples {std::make_unique<std::vector<long double>>(1)},
               a {1}, b {0}, end_flag {true} {}

Task::Task(long double (*f)(long double),
           std::unique_ptr<std::vector<long double>> &&samples,
           const long double a,
           const long double b)
	: f {f}, samples {std::move(samples)}, a {a}, b {b}, end_flag {false} {}

MapToken::MapToken(long double (*f)(long double),
                   std::unique_ptr<std::vector<long double>> &&samples,
                   const long double interval_length,
		const bool flag)
	: f {f}, samples {std::move(samples)}, interval_length {interval_length},
	  end_flag {flag} {}

ReduceToken::ReduceToken(std::unique_ptr<std::vector<long double>> &&samples,
                         const long double interval_length,
                         const bool flag) :
	samples {std::move(samples)}, interval_length {interval_length},
	end_flag {flag} {}

ResultPrinterToken::ResultPrinterToken(const long double value,
                                       const long double interval_length,
                                       const unsigned long n,
                                       const bool flag)
	: value {value}, interval_length {interval_length},
	  number_of_samples {n}, end_flag {flag} {}
