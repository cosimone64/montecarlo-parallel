/*
 * Parallel Montecarlo integration implementation
 *
 * Copyright (C) 2018-2021, cosimone64
 *
 * This program is free software, released under the GNU GPLv3
 * (General Public License).
 *
 * You should have received a copy of the GNU GPLv3 with this software,
 * if not, please visit <https://www.gnu.org/licenses/gpl.html>
 */
#ifndef SUPERTHREADED_MODULES_GUARD
#define SUPERTHREADED_MODULES_GUARD

#include <cstdint>
#include <cmath>
#include <fstream>
#include <iostream>
#include <limits>
#include <random>
#include <streambuf>
#include <string>
#include <thread>
#include <unordered_map>
#include <unistd.h>
#include <utility>
#include <vector>

#include "superthreadedtypes.hpp"
#include "commonops.hpp"

/*
 * NB: Inline functions must have the full definition visible
 * at compile time, therefore they too are placed in a header file.
 */

/*
 * Returns a random number representing the number of samples
 * to use to compute the integral.
 */
inline std::uint_fast32_t
get_number_of_samples()
{
	static constexpr std::uint_fast32_t MAX_N {1000000ul};
#ifndef TEST
	static std::uint_fast16_t MIN_N {512ul};
	static std::random_device rd;
	static std::mt19937 rng {rd()};
	static std::uniform_int_distribution<std::uint_fast32_t> distr {
		MIN_N, MAX_N};
	return static_cast<std::uint_fast32_t>(distr(rng));
#else
	return MAX_N;
#endif
}
/*
 * Explicitly sets CPU affinity for thread th on CPU with ID id.
 * This makes sure all threads run on the same CPU. Workers
 * in particular are mapped to contigous CPUs.
 *
 * If the specified thread id is larger or equal to the maximum number
 * of threads supported by the platform, the function returns
 * immediately and does nothing.
 */
inline void
set_affinity(const std::uint_fast16_t id, std::thread &th)
{
	static const std::uint_fast16_t max_threadnum {std::thread::hardware_concurrency()};
	if (id >= max_threadnum)
		return;
	cpu_set_t cpuset;
	CPU_ZERO(&cpuset);
	CPU_SET(id, &cpuset);
	if (pthread_setaffinity_np(th.native_handle(), sizeof(cpu_set_t), &cpuset)) {
		std::cerr << "Error while setting affinity on CPU "
			<< id << std::endl;
		std::exit(EXIT_FAILURE);
	}
}
/*
 * Returns the corresponding output stream object to write results to.
 * IMPORTANT: move semantics for streams are correcly implemented only
 * from the C++17 standard and beyond, so make sure to compile with a
 * C++17-compliant compiler!
 */
inline std::ostream
get_output_stream(const std::string &outputfilename)
{
	const std::ofstream fileoutputstream {outputfilename};
	std::streambuf *buf {outputfilename != "" ?
		fileoutputstream.rdbuf() : std::cout.rdbuf()};
	return std::ostream {buf};
}
/*
 * Parses the function text token and returns a function
 * pointer to use the required function.
 */
template<typename FloatType>
inline auto
get_function(const std::string &name) -> auto (*)(FloatType) -> FloatType
{
	static constexpr auto function_error =
		"Error: specified function does not exist";
	if (name == "square") {
		return [](FloatType x) { return x * x; };
	} else if (name == "cube") {
		return [](FloatType x) { return x * x * x; };
	} else if (name == "sin") {
		return std::sin;
	} else {
		std::cerr << function_error << std::endl;
		exit(EXIT_FAILURE);
	}
	return nullptr;
}
/*
 * Main scatter function. Sends a copy of the same task to each
 * worker until there are valid lines in the input file. At the end,
 * it sends a final, dummy task to each worker with end flag set to
 * true, the other values are irrelevant. This is necessary to signal the
 * end of the input stream.
 */
template<typename FloatType>
inline void
scatter(const std::string &filename,
        std::vector<McQueue<Task<FloatType>>> &task_queues)
{
	std::uint_fast32_t line_num {0};
	std::ifstream inputfile {filename};
	std::string function_name;
	FloatType a;
	FloatType b;

	while (inputfile >> function_name >> a >> b) {
		++line_num;
		const auto sample_num = get_number_of_samples();
		FloatType (*func)(FloatType) {get_function<FloatType>(function_name)};
		for (auto &q : task_queues) {
			Task<FloatType> task {func, a, b, sample_num,
				line_num, false};
			q.put(std::move(task));
		}
	}
	for (auto &q : task_queues) {
		Task<FloatType> last_task {std::sin, 0, 1, 1, 0, true};
		q.put(std::move(last_task));
	}
}
/*
 * Worker function. Reads parameters in the current task
 * and computes the local result, which is obtained by
 * summing f(x) over all the local samples. Random samples
 * are computed "on the fly" without storing them in an
 * array first, they are simply added to the local result
 * once f(x) is computed.
 */
template<typename AccumulatorType, typename FloatType>
inline void
work(const std::uint_fast32_t workernum,
     const std::uint_fast32_t worker_id,
     const std::uint_fast32_t worker_seed,
     McQueue<Task<FloatType>> &input_queue,
     McQueue<WorkerOutputToken<AccumulatorType, FloatType>> &output_queue)
{
	bool end_flag {false};
	std::mt19937 engine {worker_seed};

	while (!end_flag) {
		const auto task = std::move(input_queue.get());
		end_flag = task.end_flag;
		std::uniform_real_distribution<FloatType> distr {task.a, task.b};
		const std::uint_fast32_t partition_size {task.number_of_samples / workernum};
		const std::uint_fast32_t start_index {partition_size * worker_id};
		const std::uint_fast32_t end_index {
			worker_id == workernum - 1 ?
				task.number_of_samples
				: partition_size * (worker_id + 1)};
		AccumulatorType local_result {0};

		for (std::uint_fast32_t i {start_index}; i < end_index; ++i)
			local_result += task.f(distr(engine));
		const FloatType interval_length {task.b - task.a};
		WorkerOutputToken<AccumulatorType, FloatType> output_token {
					local_result, interval_length,
					task.number_of_samples, task.id,
					end_flag};
		output_queue.put(std::move(output_token));
	}
}
/*
 * Computes the full result from a std::vector of partial results and the remaining
 * parameters of the current task, and then prints it to output.
 */
template<typename AccumulatorType, typename FloatType>
inline void
print_full_result(const std::vector<AccumulatorType> &partial_results,
                  const WorkerOutputToken<AccumulatorType, FloatType> &task,
                  std::ostream &output_stream)
{
#ifndef TEST
	constexpr auto separator = '\n';
#endif
	AccumulatorType accumulated_result {0};
	for (const auto &partial_result : partial_results)
		accumulated_result += partial_result;
	const AccumulatorType final_result {
		(accumulated_result * task.interval_length)
			/ task.number_of_samples};
#ifndef TEST
	output_stream << final_result << separator;
#endif
}
/*
 * Main gatherer function. It stores partial results in a hashmap
 * mapping token ids to respective std::vectors of partial results.
 * When enough partial results have been received for a single
 * id, the respective full result is printed to output and the
 * corresponding std::vector deallocated.
 *
 * Unlike the FastFlow version, it needs to check whether the
 * current task is not the final dunny task when enough results have been
 * received.
 */
template<typename AccumulatorType, typename FloatType>
inline void
gather(const std::string &outputfilename,
       std::vector<McQueue<WorkerOutputToken<
       AccumulatorType, FloatType>>> &task_queues)
{
	const std::uint_fast16_t workernum {task_queues.size()};
	auto output_stream = get_output_stream(outputfilename);
	std::unordered_map<std::uint_fast32_t, std::vector<AccumulatorType>> output_map;
	std::vector<bool> end_flags (workernum, false);
	const auto are_all_flags_true = [&] {
		for (const auto &flag : end_flags)
			if (!flag) return false;
		return true; };
	std::uint_fast32_t worker_index {0};

	for (; !are_all_flags_true(); worker_index = (worker_index + 1) % workernum) {
		const auto task = std::move(task_queues[worker_index].get());
		if (task.end_flag)
			end_flags[worker_index] = true;

		auto vector_pos = output_map.find(task.id);
		if (vector_pos == output_map.end()) {
			output_map[task.id] = std::vector<AccumulatorType> {};
			output_map[task.id].reserve(workernum);
			vector_pos = output_map.find(task.id);
		}
		auto &result_vector = vector_pos->second;
		result_vector.push_back(task.partial_result_value);
		if (result_vector.size() == workernum && !task.end_flag) {
			print_full_result(result_vector, task, output_stream);
			output_map.erase(vector_pos);
		}
	}
}
/*
 * Starts the scatterer thread, sets affinity and returns the corresponding
 * thread object. No copy is performed, since the object is explicitly moved
 * through move semantics. This avoids the dangerous possibility
 * of creating another thread.
 */
template<typename FloatType>
inline std::thread
start_scatterer(const std::uint_fast16_t scatterer_id,
                const std::string &inputfilename,
                std::vector<McQueue<Task<FloatType>>> &queues)
{
	std::thread scatterer_thread {scatter<FloatType>, std::ref(inputfilename),
		std::ref(queues)};
	set_affinity(scatterer_id, scatterer_thread);
	return scatterer_thread;
}

/*
 * Starts a number of additional workers equal to workernum - 1
 * and returns a std::vector containing them.
 */
template<typename AccumulatorType, typename FloatType>
inline std::vector<std::thread>
start_workers(const std::uint_fast16_t workernum,
              std::vector<McQueue<Task<FloatType>>> &input_queues,
              std::vector<McQueue<
              WorkerOutputToken<AccumulatorType, FloatType>>> &output_queues)
{
	const std::vector<std::uint_fast16_t> worker_seeds {get_worker_seeds(workernum)};
	std::vector<std::thread> worker_threads;
	worker_threads.reserve(workernum);

	for (std::uint_fast16_t thread_id {0}; thread_id < workernum; ++thread_id) {
		worker_threads.emplace_back(work<AccumulatorType, FloatType>,
				workernum, thread_id, worker_seeds[thread_id],
				std::ref(input_queues[thread_id]),
				std::ref(output_queues[thread_id]));
		set_affinity(thread_id, worker_threads[thread_id]);
	}
	return worker_threads;
}

/*
 * Same thing, except for the gatherer thread.
 */
template<typename AccumulatorType, typename FloatType>
inline std::thread
start_gatherer(const std::uint_fast16_t gatherer_id,
               const std::string &outputfilename,
               std::vector<McQueue<
               WorkerOutputToken<AccumulatorType, FloatType>>> &queues)
{
	std::thread gatherer_thread {gather<AccumulatorType, FloatType>,
		std::ref(outputfilename),
		std::ref(queues)};
	set_affinity(gatherer_id, gatherer_thread);
	return gatherer_thread;
}

#endif
