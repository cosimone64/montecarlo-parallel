/*
 * Parallel Montecarlo integration implementation
 *
 * Copyright (C) 2018-2021, cosimone64
 *
 * This program is free software, released under the GNU GPLv3
 * (General Public License).
 *
 * You should have received a copy of the GNU GPLv3 with this software,
 * if not, please visit <https://www.gnu.org/licenses/gpl.html>
 */
#ifndef FF_MODULES_GUARD
#define FF_MODULES_GUARD

#include <iostream>
#include <fstream>
#include <string>
#include <memory>
#include <ff/node.hpp>
#include <ff/parallel_for.hpp>
#include <ff/map.hpp>

/*
 * POD types, the tasks sent and received among pipeline nodes.
 */
struct SamplerTask
{
	long double (*f)(long double);
	std::unique_ptr<std::vector<long double>> samples;
	const long double a;
	const long double b;

	SamplerTask(long double (*f)(long double),
	            std::unique_ptr<std::vector<long double>> &&samples,
	            const long double a,
	            const long double b);
};

struct MapperTask
{
	long double (*f)(long double);
	std::unique_ptr<std::vector<long double>> samples;
	const long double interval_length;

	MapperTask(long double (*f)(long double),
	           std::unique_ptr<std::vector<long double>> &&samples,
	           const long double interval_length);
};

struct ReducerTask
{
	std::unique_ptr<std::vector<long double>> samples;
	const long double interval_length;

	ReducerTask(std::unique_ptr<std::vector<long double>> &&samples,
	            const long double interval_length);
};

struct ResultPrinterTask
{
	const long double partial_result;
	const long double interval_length;
	const unsigned long n;
};

/*
 * Parallel modules.
 */
class StreamGenerator : public ff::ff_node_t<SamplerTask>
{
	const std::string filename;
	const unsigned min_number_of_samples;

	inline long double
	(*get_function(const std::string &name))(long double);

	inline unsigned long
	get_number_of_samples(unsigned min_n);
public:
	StreamGenerator(const std::string &filename,
	                const unsigned reducer_degree);

	SamplerTask *
	svc(SamplerTask *);
};

class Sampler : public ff::ff_Map<SamplerTask, MapperTask>
{
	const unsigned degree;
public:
	Sampler(const unsigned degree);

	MapperTask *
	svc(SamplerTask *input);
};

class Mapper : public ff::ff_Map<MapperTask, ReducerTask>
{
	const unsigned degree;
public:
	Mapper(const unsigned degree);

	ReducerTask *
	svc(MapperTask *input);
};

class Reducer : public ff::ff_node_t<ReducerTask, ResultPrinterTask>
{
	const unsigned degree;
	ff::ParallelForReduce<long double> pfr;
public:
	Reducer(const unsigned degree);

	ResultPrinterTask *
	svc(ReducerTask *input);
};

class ResultPrinter : public ff::ff_node_t<ResultPrinterTask, long double>
{
	const char separator;
	const std::ofstream fileoutputstream;
	std::ostream output_stream;
public:
	ResultPrinter(const std::string &outputfilename);

	long double *
	svc(ResultPrinterTask *input);
};

#endif
