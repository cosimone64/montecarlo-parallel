/*
 * Parallel Montecarlo integration implementation
 *
 * Copyright (C) 2018-2021, cosimone64
 *
 * This program is free software, released under the GNU GPLv3
 * (General Public License).
 *
 * You should have received a copy of the GNU GPLv3 with this software,
 * if not, please visit <https://www.gnu.org/licenses/gpl.html>
 */
#include <memory>
#include <vector>

#include "threadtypes.hpp"
#include "paralleltemplate.hpp"

/*
 * Template specializations for worker functions.
 */
template<>
void ParallelStage<Task, MapToken>::update_buffer(
		const unsigned long start_index,
		const unsigned long end_index,
		const Task &current_input_token)
{
	thread_local static std::random_device rd;
	thread_local static std::mt19937 eng {rd()};
	std::vector<long double> &samples {*(current_input_token.samples)};
	std::uniform_real_distribution<long double> distr {
		current_input_token.a, current_input_token.b};
	for (unsigned long i {start_index}; i < end_index; ++i)
		samples[i] = distr(eng);
}

template<>
void ParallelStage<MapToken, ReduceToken>::update_buffer(
		const unsigned long start_index,
		const unsigned long end_index,
		const MapToken &current_input_token)
{
	std::vector<long double> &samples {*(current_input_token.samples)};
	const auto &f {current_input_token.f};
	for (unsigned long i {start_index}; i < end_index; ++i)
		samples[i] = f(samples[i]);
}

template<>
void ParallelStage<ReduceToken, ResultPrinterToken>::update_buffer(
		const unsigned long start_index,
		const unsigned long end_index,
		const ReduceToken &current_input_token)
{
	std::vector<long double> &samples {*(current_input_token.samples)};
	for (unsigned long i {start_index + 1}; i < end_index; ++i)
		samples[start_index] += samples[i];
}


/*
 * Template specializations for output generating functions.
 */
template<>
std::unique_ptr<MapToken> ParallelStage<Task, MapToken>::get_output_token(
		Task &current_input_token)
{
	return std::make_unique<MapToken>(current_input_token.f,
			std::move(current_input_token.samples),
			(current_input_token.b - current_input_token.a),
			current_input_token.end_flag);
}

template<>
std::unique_ptr<ReduceToken> ParallelStage<MapToken, ReduceToken>::get_output_token(
		MapToken &current_input_token)
{
	return std::make_unique<ReduceToken>(std::move(current_input_token.samples),
			current_input_token.interval_length,
			current_input_token.end_flag);
}

template<>
std::unique_ptr<ResultPrinterToken>
ParallelStage<ReduceToken, ResultPrinterToken>::get_output_token(
		ReduceToken &current_input_token)
{
	const std::vector<long double> &buffer {*(current_input_token.samples)};
	const unsigned long partition_size {buffer.size() / workernum};
	long double value {0};
	for (unsigned i {0}; i < workernum; ++i)
		value += buffer[i * partition_size];
	return std::make_unique<ResultPrinterToken>(value,
			current_input_token.interval_length,
			current_input_token.samples->size(),
			current_input_token.end_flag);
}

#endif
