/*
 * Parallel Montecarlo integration implementation
 *
 * Copyright (C) 2018-2021, cosimone64
 *
 * This program is free software, released under the GNU GPLv3
 * (General Public License).
 *
 * You should have received a copy of the GNU GPLv3 with this software,
 * if not, please visit <https://www.gnu.org/licenses/gpl.html>
 */
#include <iostream>
#include <fstream>
#include <streambuf>
#include <memory>
#include <thread>
#include <random>
#include <string>
#include <unordered_map>
#include <cmath>
#include <cstdlib>
#include <unistd.h>

#include "threadtypes.hpp"
#include "threadmodules.hpp"
#include "paralleltemplate.hpp"

void
parse_args(const int &argc, char **&argv,
           std::unordered_map<char, unsigned> &degrees,
           unsigned short &async_degree,
           std::string &inputfilename,
           std::string &outputfilename);

inline long double
(*get_function(const std::string &name))(long double);

inline unsigned long
get_number_of_samples(unsigned min_n);

inline void
send_tasks(const std::string &filename, unsigned reducer_degree,
           SynchronizedQueue<Task> &task_queue);

void
get_final_result(const std::string &outputfilename,
                 SynchronizedQueue<ResultPrinterToken> &input_queue);


int
main(int argc, char *argv[])
{
	if (argc < 2) {
		std::cerr << "Use as " << argv[0]
			<< " <degrees> <filename>" << std::endl;
		return EXIT_FAILURE;
	}
	std::unordered_map<char, unsigned> degrees {{'s', 1},
		{'m', 1}, {'r', 1}};
	unsigned short async_degree {1};
	std::string inputfilename {""};
	std::string outputfilename {""};
	parse_args(argc, argv, degrees, async_degree, inputfilename, outputfilename);

	SynchronizedQueue<Task> input_to_sampler_queue {async_degree};
	SynchronizedQueue<MapToken> sampler_to_mapper_queue {async_degree};
	SynchronizedQueue<ReduceToken> mapper_to_reducer_queue {async_degree};
	SynchronizedQueue<ResultPrinterToken> reducer_to_result_printer_queue {
		async_degree};

	Sampler sampler {degrees['s'], input_to_sampler_queue,
		sampler_to_mapper_queue};
	std::thread sampler_thread {&Sampler::run, &sampler};
	Mapper mapper {degrees['m'], sampler_to_mapper_queue,
		mapper_to_reducer_queue};
	std::thread mapper_thread {&Mapper::run, &mapper};
	Reducer reducer {degrees['r'], mapper_to_reducer_queue,
		reducer_to_result_printer_queue};
	std::thread reducer_thread {&Reducer::run, &reducer};
	std::thread result_printer_thread {&get_final_result, outputfilename,
		std::ref(reducer_to_result_printer_queue)};

	send_tasks(inputfilename, degrees['r'], input_to_sampler_queue);

	const std::vector<std::thread *> thread_array {&sampler_thread,
		&mapper_thread, &reducer_thread, &result_printer_thread};
	for (const auto &t : thread_array) {
		if (t->joinable())
			t->join();
	}
	return 0;
}
void
parse_args(const int &argc, char **&argv,
           std::unordered_map<char, unsigned> &degrees,
           unsigned short &async_degree,
           std::string &inputfilename,
           std::string &outputfilename)
{
	bool error_found {false};
	if (argc < 2)
		error_found = true;
	int c;
	while ((c = getopt(argc, argv, "a:s:m:r:d:i:o:")) != -1) {
		if (c == 'a') {
			for (auto &v : degrees)
				v.second = std::atoi(optarg);
		} else if (c == 's' || c == 'm' || c == 'r') {
			degrees[c] = std::atoi(optarg);
		} else if (c == 'd') {
			async_degree = std::atoi(optarg);
		} else if (c == 'i') {
			inputfilename = optarg;
		} else if (c == 'o') {
			outputfilename = optarg;
		} else {
			error_found = true;
		}
	}
	if (degrees['s'] < 1 || degrees['m'] < 1 || degrees['r'] < 1) {
		std::cerr << "Error: number of workers must be at least 1" << std::endl;
		error_found = true;
	}
	if (async_degree < 1) {
		std::cerr << "Error: async degree must be at least 1" << std::endl;
		error_found = true;
	}
	if (inputfilename == "") {
		std::cerr << "Error, no input file specified" << std::endl;
		error_found = true;
	}
	if (error_found) {
		std::cerr << "Use as " << argv[0]
			<< " <degrees> -i <inputfilename> [-o <outputfilename>]"
			<< std::endl;
		exit(EXIT_FAILURE);
	}
}
void
send_tasks(const std::string &filename, const unsigned reducer_degree,
           SynchronizedQueue<Task> &task_queue)
{
	static constexpr char INTERVAL_ERROR[] {"Error: first interval"
		"extreme (a) is larger than the second one (b) in line: "};
	const unsigned min_number_of_samples {reducer_degree * reducer_degree};
	std::ifstream file {filename};
	std::string function_name;
	unsigned line_num {0};
	long double a, b;

	while (file >> function_name >> a >> b) {
		line_num++;
		if (a > b) {
			std::cerr << INTERVAL_ERROR << line_num << std::endl;
			std::exit(EXIT_FAILURE);
		}
		const unsigned long samples_num {
			get_number_of_samples(min_number_of_samples)};
		auto task {std::make_unique<Task>(
				get_function(function_name),
				std::make_unique<std::vector<long double>>(samples_num),
				a, b)};
		task_queue.put(std::move(task));
	}
	auto last_task {std::make_unique<Task>()};
	task_queue.put(std::move(last_task));
}
long double
(*get_function(const std::string &name))(long double)
{
	static constexpr char FUNCTION_ERROR[] {
		"Error: specified function does not exist"};
	if (name == "square") {
		return [](long double x) { return x * x; };
	} else if (name == "cube") {
		return [](long double x) { return x * x * x; };
	} else if (name == "sin") {
		return std::sin;
	} else {
		std::cerr << FUNCTION_ERROR << std::endl;
		std::exit(EXIT_FAILURE);
	}
	return nullptr;
}
unsigned long
get_number_of_samples(const unsigned min_n)
{
	static constexpr unsigned long MAX_N {1000000ul};
#ifndef TEST
	static std::random_device rd;
	static std::mt19937 rng {rd()};
	static std::uniform_int_distribution<unsigned long> distr {
		min_n, MAX_N};
	return distr(rng);
#else
	return MAX_N;
#endif
}
/*
 * Final result-averaging function.
 */
void
get_final_result(const std::string &outputfilename,
                 SynchronizedQueue<ResultPrinterToken> &input_queue)
{
	constexpr char separator {'\n'};
	const std::ofstream fileoutputstream {outputfilename};
	std::streambuf *buf {outputfilename != "" ?
		fileoutputstream.rdbuf() : std::cout.rdbuf()};
	std::ostream output_stream {buf};
	bool end_stream {false};

	while (!end_stream) {
		std::unique_ptr<const ResultPrinterToken> token {std::move(input_queue.get())};
		if (!token->end_flag) {
			const long double &length {token->interval_length};
			const unsigned long &n {token->number_of_samples};
			output_stream << token->value * length / n << separator;
		}
		end_stream = token->end_flag;
	}
}
