# Montecarlo integration

A high-performance parallel implementation of a nummerical
integral approximation via the Montecarlo method.

Developed using both standard C++ mechanisms and the
FastFlow framework: http://calvados.di.unipi.it/

# Overview

This application computes the integral of a single-variable function over an interval
using a *Montecarlo*
method: given a function $f\left(x\right)$ to be integrated in the interval
$\left[a,b\right]$, we choose a number $N$ of random points
$h_{i}$, $i\in\left[1,N\right]$, $h_{i}\in\left[a,b\right]$,
and we approximate the integral $\int_{a}^{b}f\left(x\right)dx$ as

$$
\frac{1}{N}\sum_{i=1}^{N}f\left(x_{i}\right)\left(b-a\right)
$$


The integral is computed over a stream of intervals
$$
\left<\left(f_{1},a_{1},b_{1}\right),\left(f_{2}, a_{2}, b_{2}\right),
\ldots\left(f_{n},a_{n},b_{n}\right),\ldots\right>
$$
which is more general than a stream of the form
$$
\left<\left(a_{1},b_{1}\right),\left(a_{2}, b_{2}\right),
\ldots\left(a_{n},b_{n}\right),\ldots\right>
$$
with $f$ fixed (the latter can be obtained from the former simply by
setting $f_{1}=f_{2}=\ldots=f_{n}=f_{n+1}\ldots$

The parallel computation has been implemented using both standard C++
threads and the FastFlow framework. The two approaches feature minor
structural differences due to the different mechanisms utilized,
but they share the same fundamental ideas.


# Parallel architecture design

First of all, we can simplify the formula by taking
$\left(b-a\right)$ outside the sum, therefore computing instead
$$
\frac{\left(b-a\right)}{N}\sum_{i=1}^{N}f\left(x_{i}\right)
$$
which yields the same result, but saves $N-1$ multiplications.

\newpage

Given an individual token $\left(f,a,b\right)$ from the stream, the basic sequential
algorithm to compute an approximation of $\int_{a}^{b}f\left(x\right)dx$
using the desired *Montecarlo* method is given by the following pseudocode:

~~~
montecarlo(f, a, b)
	N <- Random natural numeber
	S <- Set of N random real numbers
	M <- {y = f(x) | x is an element of S}
	result <- Sum all elements of M
	return result * (b - a) / N
end
~~~

To further optimize the algorithm and save memory accesses, we can
"collapse" steps 3, 4 and 5 without explicitly storing results
into a buffer, therefore yielding:

~~~
montecarlo(f, a, b)
	N <- Random natural numeber
	result <- 0
	for (i <- 0; i < N; i <- i + 1)
		r <- Random natural number inside [a, b]
		result += f(r)
	return result * (b - a) / N
end
~~~

Both implementations focus on parallelizing the loop as the main
way to improve performance,
resulting in a "map-reduce-like" data parallel computation. There is an important
difference with respect to a "true" map-reduce computation, however: in this case,
threads are not working on an actual data structure, since intermediate
results are generated "on the fly" and immediately added to the
accumulated result, without storing them in memory. Data partitions among workers
are thus purely "logical".

Both implementations create a fixed number of threads which does not
change throughout the computation. Since threads are created only
once, namely, when the program starts, and joined once as well, when the program
terminates, the overall performance impact of such operations is negligible.

## Partitioning
![Task diagram with 3 workers](spmffgraph.png){width=60%}

Both versions use a "scatter-work-gather" topology to parallelize
the computation: The FastFlow version in particular uses a *farm* construct to simulate
a map-reduce computation
with a fixed number of workers: this avoids the overhead that would be caused
by continuously creating and destroying threads via `parallel_for` and
`parallel_for_reduce` constructs.

The *scatterer* fetches the next input token and sends it to
all workers, which in turn compute and send their partial results to the gatherer.
Workers compute the respective partitions of the loop, which have all the
same length except for the last worker (unless $N$ happens to be a multiple
of the number of workers, of course).

The gatherer collects output tokens from each worker and assembles the final
result.
In order to distinguish partial results belonging to different
stream items, each input token has a specific *id*, which
is preserved for each of the partial result tokens computed by the workers.
For each token it receives, the gatherer checks its id and stores its partial
result in a buffer. This buffer is indexed by the same id: the gatherer uses
a *hashmap* to find the correct one.

Once enough partial results have been received for a single input token (namely,
one from each worker) the gatherer sums them all up, multiplies it by the
interval length, divides it by the number of samples, and ultimately
prints the result. All this information is received from the workers.

The messages exchanged can be of two types: From scatterer to workers: $(f, a, b, N, id)$, where

$f$
: Function to be integrated.

$a$, $b$
: Left and right extremes of the integration interval, respectively.

$N$
: Number of samples to use for the computation.

$id$
: A unique token id, necessary to distinguish partial results.

And from workers to gatherer: $(v, l, N, id)$, where

$v$
: Partial result computed by the worker.

$l$
: $(b - a)$, the interval length, necessary to compute the final result.

$N$
: Number of samples, same as the corresponding input token.

$id$
: Token id, same as the corresponding input token.

In the C++ thread version, these types both feature an additional boolean
flag to signal the end of the stream. This is not required for the FastFlow
version, since this can be achieved by propagating the special value EOS.

## Communications

The C++ thread version uses a basic queue implemented using a circular
array, each of them used in a "producer-consumer"-like fashion by exactly
two threads (One thread stores, one thread retrieves).
Synchronization is lock-free, relying on atomic operations on the
*head* and *tail* indices. The FastFlow version relies on the
communication mechanisms implicitly offered by the framework.


# Performance modeling

For both implementations, the *compute* phase is the main source of parallelism,
therefore it is also the most impactful phase on the overall performance.

Let us define the following:

$T_{f}$
: Average time required to fetch the next input token and send it to the workers.

$T_{c}$
: Average time required to sample a random number in the interval, compute
the function on it and sum the resulting value to the accumulated result.

$T_{p}$
: Average time to compute the remaining arithmetic operations and print the
result.

$\boldsymbol{N}$
: Random variable representing the number of samples to compute.

$N$
: $E\left\{\boldsymbol{N}\right\}$

The total latency for the sequential version is given by

$$
T_{seq} = T_{f} + NT_{c} + T_{p}
$$

While the latency for the parallel versions, since each
worker performs approximately $\frac{N}{n}$ basic operations
having cost $T_{c}$, is

$$
L_{par}\approx T_{f} + \frac{N}{n}T_{c} + T_{p}
$$


## Service time

Emitter and collector are separate threads, hence
fetching an input item and printing a result happens in parallel with
respect to the main computation. We can therefore see the computation graph
as a 3-stage pipeline, whose service time with n workers is,
if $N$ is large enough,
 
$$
T\left(n\right)=max\left(T_{f}, \frac{NT_{c}}{n}, T_{p}\right)=\frac{NT_{c}}{n}
$$

The ideal speedup is therefore given by

$$
speedup(n)_{ideal}=\frac{NT_{c}}{\frac{N}{n}T_{c}}=n
$$


# Experimental validation

The following plots consider the number of *workers* used, hence
three extra threads are actually used: one for the scatterer, one for
the gatherer and one for the main thread. These tests are executed on a
stream of over 200,000 intervals (217,854 to be exact) using simple functions
only, such as $f\left(x\right) = x^{2}$, $f\left(x\right) = x^{3}$ and
$f\left(x\right) = \sin\left(x\right)$. In order to obtain predictable,
comparable results, $N$ is fixed at 1,000,000 for every interval. As for
the *scalability*, the reference version uses a single worker.

![](threadtime.png){width=50%} ![](fftime.png){width=50%}
\begin{figure}[!h]
	\caption{Completion time for the C++ thread version (left) and the FastFlow version (right)}
\end{figure}

![](threadspeedup.png){width=50%} ![](ffspeedup.png){width=50%}
\begin{figure}[!h]
	\caption{Speedup for the C++ thread version (left) and the FastFlow version (right)}
\end{figure}

![](threadscalability.png){width=50%} ![](ffscalability.png){width=50%}
\begin{figure}[!h]
	\caption{Scalability for the C++ thread version (left) and the FastFlow version (right)}
\end{figure}

![](threadefficiency.png){width=50%} ![](ffefficiency.png){width=50%}
\begin{figure}[!h]
	\caption{Efficiency for the C++ thread version (left) and the FastFlow version (right)}
\end{figure}

Both versions initially perform close to the expected optimal performance,
peaking at around ~56 threads. After that, performance starts to worsen,
then it stabilizes again. A slightly worse performance can be explained by the
additional number of memory allocations and the added load on the operating
system.

It's interesting to note how both versions initially display an efficiency slightly
greater than 1: this is because workers can start computing the respective
partition of the next input token once they finish computing the
current one: this is particularly important for the *last worker*, which computes
a smaller partition. Since it finishes earlier than the others, it can also
start earlier on the next token. This effect gradually becomes
negligible as workers are added, since the difference in partition
size for this last worker diminishes.

In order to get results as comparable as possible, the C++ thread version
manually sets the *affinity* for each thread, forcing them to run on logically
"contiguous" processors. The FastFlow version does this automatically.


## Pitfalls

The FastFlow version dynamically allocates on the heap every object exchanged
for communication. This cost adds up as the stream length increases, further
decreasing performance. Neither the C++ thread version nor the sequential version
suffer from this problem, since the former allocates everything on the stack
and relies on C++ *move semantics* when storing message in the
queues, while the latter obviously doesn't need any communications. Allocating
on the heap is more expensive, and that's the main cause of the slightly worse
performance of the FastFlow version.


# Implementation structure

Common functions used by both implementations are defined in a header file named
*commonops.hpp*.

## C++ thread version

In addtion to the common *commonops.hpp* header, this version is composed of
the following files:

`threadtypes.hpp`
: *POD* structs representing the messages exchanged.

`threadmodules.hpp`
: Defines the main functions used by the parallel modules.

`threadmontecarlo.cpp`
: Contains the `main` function, which initializes all relevant data structures,
starts the threads and joins them.

## FastFlow version

Similarly, the FastFlow version is composed of three files in addition to
the common *commonops.hpp* header file;

`fftypes.hpp`
: *POD* structs representing the messages exchanged, same as the C++ thread
version, but without the end stream flag, since it's not needed. Inheritance
is not used since it would have been overkill for such a simple case.

`ffmodules.hpp`
: Defines the classes which represent the parallel modules (scatterer, workers,
gatherer), along with their
relative functions.

`ffmontecarlo.cpp`
: Contains the `main` function, which initializes all relevant data structures,
starts the threads and joins them.

The sequential version consists of a single file, *sequentialmontecarlo.cpp*.

Timing is done via the `chrono` library, which is part of the C++ standard
library, and enabled by defining the macro `TEST`. This also disables
writing results to output, to get a more accurate estimate of the actual
computation time.

The code is heavily templated: this is to allow greater flexibility in
choosing the desired floating point types depending on how much precision
is needed: the experiments use `float` types for individual random samples and
`double`s for accumulating them.

## Assumptions and limitations

For the sake of performance and simplicity, some assumptions and
simplifications have been made to the program, namely:

+ the input file is assumed to be well-formed: the stream
generator does not check for syntax errors when generating tokens;

+ $N$ is assumed to be larger than the number of workers in each
individual stage;

+ as it is, the program supports only three functions: $f\left(x\right)=x^{2}$,
$f\left(x\right)=x^{3}$ and $f\left(x\right) = sin\left(x\right)$.
Extending the program to support more functions is possible by augmenting
the function-generating\ldots function, possibly by adding more cases or
even implementing a complete parser. This is beyond the scope of the problem,
however, so it has been omitted.

# User manual
Input files must have tokens represented by lines in the form
`<function name> <a> <b>`,
all separated by spaces and each token separated by a newline.
Both executables, namely `threadmontecarlo` and `ffmontecarlo`
may be run with the following options:

`-d`
: Sets the number of workers (default 1).

`-i`
: Sets the input file.

`-o`
: Sets the output file (default stdout).

All parameters except `-i` are optional.
In case, for any reason, options are set multiple times,
the one specified *last* overrides the previous ones. The sequential version
may be run as `./sequentialmontecarlo <inputfile>`.

A set of predefined tests can be run by executing the shell script
`./runtests.sh <inputfile>` included in the source directory. Some sample
input files are also included. The plots included in this document
can be generated via the gnuplot script `generateplots.gp` included in the
`testdata` folder.


## Build process
A makefile named `phi.make` is provided in the source directory. All that is
needed to build all three executables (including the sequential version) is
running the command `make -f phi.make`. Individual targets may be specified.

The `test` version of all implementations (the one used for the experiments,
with $N$ fixed at 1,000,000) can be generated by running
`make -f phi.make test`
